export interface JWTOptions {
  auth?: string;
  audience?: string;
  callbackUrl?: string;
}
