const ethUtils = require("ethereumjs-util");

import * as didJWT from "./JWT";
import { SimpleSigner } from "./SimpleSigner";
import { Either, Right } from "monet";

import { buildDIDString, parseDID } from "./services/did";

import * as signatureService from "./services/signature";

import { validateDIDDetails, validatePlaintextDID } from "./validations/did";

import { DIDDetails } from "./models/didDetails";

const getOrThrow = (either: Either<Error, any>) => {
  return either.cata(err => {
    throw err;
  }, result => result);
};

/**
 *  Generates a public/private key pair
 *  @example
 *  createKeyPair()
 *
 *  @return   {Object}                               a JS object representing a valid key pair
 */
function createKeyPair() {
  const privateKey = signatureService.randomString(64);
  return {
    public: ethUtils.privateToPublic("0x" + privateKey).toString("hex"),
    private: privateKey,
    address: ethUtils.privateToAddress("0x" + privateKey).toString("hex")
  };
}

/**
 *  Signs a list of data with a private key.  The data is concatinated, hashed, and signed with a
 *  privateKey
 *
 *  @example
 *  signData(["data1", "data2", "data3"], "37e31a0ae821774...")
 *
 *  @param    {Array<string>}     args               data array to be signed
 *  @param    {String}            privateKey         private key to sign data
 *  @return   {String}                               a signature string
 */
function signData(args: string[], privateKey: string): string {
  const signWithKey = signatureService.signHashed(privateKey);
  return getOrThrow(
    signatureService
      .concatData(args)
      .chain(signatureService.hashData)
      .chain(signWithKey)
  );
}

/**
 *  Verifies the signature on a set of data.
 *  @example
 *  verifyData(["data1", "data2", "data3"], "234fa2342...", "37e31a0ae821...")
 *
 *  @param    {Array<string>}     args               data array to be verified
 *  @param    {String}            signature          signature of signed data
 *  @param    {String}            publicKey          public key used to sign data
 *  @return   {Boolean}                              true of false if public key matches recovered
 *                                                   key from the signature
 */
function verifyData(args: string[], signature: string, publicKey: string) {
  const recoverWithSig = signatureService.recoverPublicKey(signature);
  return getOrThrow(
    signatureService
      .concatData(args)
      .chain(signatureService.hashData)
      .chain(recoverWithSig)
      .chain(recoveredKey => Right(recoveredKey.toString("hex") === publicKey))
  );
}

/**
 *  Recover public key from a signature and a set of data.
 *  @example
 *  recoverPublicKey(["data1", "data2", "data3"],  "234fa2342...")
 *
 *  @param    {Array<string>}     args               data array to be verified
 *  @param    {String}            signature          signature of signed data
 *  @return   {String}                               returns public key used to sign data
 *
 */
function recoverPublicKey(args: string[], signature: string) {
  const recoverWithSig = signatureService.recoverPublicKey(signature);
  return getOrThrow(
    signatureService
      .concatData(args)
      .chain(signatureService.hashData)
      .chain(recoverWithSig)
      .chain(recoveredKey => {
        return Right(recoveredKey.toString("hex"));
      })
  );
}

/**
 *  Creates a did from input
 *  @example
 *  createDID({method: "life", version: "1", network: "6666", key: "18472392359286af2b163b" })
 *
 *  @param    {Object}            inputData          an object of DID data containing the method,
 *                                                   network, version and key(a hashed public key)
 *  @return   {String}                               DID created from data
 *
 */
function createDID(inputData: DIDDetails): string {
  return getOrThrow(validateDIDDetails(inputData).chain(buildDIDString));
}

/**
 *  Verifies that a DID is correctly formed
 *  @example
 *  validateDID("did:life:112341523dfdf90980982303b2")
 *
 *  @param    {String}            did                a DID string to validate
 *  @return   {Boolean}                              true if did is valid
 *
 */
function validateDID(did: string): boolean {
  return getOrThrow(
    validatePlaintextDID(did)
      .chain(parseDID)
      .chain(validateDIDDetails)
      .chain(result => Right(!!result))
  );
}

/**
 *  Parses a DID and extracts components
 *  @example
 *  decodeDID("did:life:112341523dfdf90980982303b2")
 *
 *  @param    {String}            did                a DID string to validate
 *  @return   {Boolean}                              An object containing DID components
 *                                                   { method: string,
 *                                                     network: string,
 *                                                     version: string,
 *                                                     key: string }
 */
function decodeDID(did: string): DIDDetails {
  return getOrThrow(
    validatePlaintextDID(did)
      .chain(parseDID)
      .chain(validateDIDDetails)
  );
}

/**
 *  Creates a signed JSON Web Token issued by a DID
 *
 *  @example
 *  createJWT("did:life:112341523dfdf90980982303b2", {payload: "some data"}, "37e31a0ae8217746722a4...").then(jwt => {
 *      ...
 *  })
 *
 *  @param    {String}            did                did to issue JWT
 *  @param    {Object}            data               payload to sign
 *  @param    {String}            privateKey         private key to sign JWT
 *  @param    {Number}            expiresIn          time from now JWT with expire
 *  @return   {Promise<Object, Error>}               a promise which resolves with a signed JSON Web Token or rejects with an error
 */
function createJWT(did, data, privateKey, expiresIn = 600000) {
  const signer = SimpleSigner(privateKey);
  return didJWT.createJWT(data, {
    alg: "ES256K-R",
    issuer: did,
    signer,
    expiresIn
  });
}

/**
 *  Verifies a signed JSON Web Token issued by a DID
 *
 *  @example
 *  verifyJWT("did:life:112341523dfdf90980982303b2").then(jwt => {
 *      ...
 *  })
 *
 *  @param    {String}            did                did to issue the JSON Web Token
 *  @param    {Object}            data               payload to sign
 *  @param    {String}            privateKey         private key to sign JWT
 *  @param    {Number}            expiresIn          time from now JWT with expire
 *  @return   {Promise<Object, Error>}               a promise which resolves with a signed JSON Web Token or rejects with an error
 */
function verifyJWT(aud, incomingJwt) {
  return didJWT.verifyJWT(incomingJwt, { audience: aud });
}

/**
 *  Register an api address to the life method for DID lookup
 *
 *  @example
 *  registerLife("http://api.lifeid.io")
 *
 *  @param    {String}            url                A URL to register
 */
function registerLife(url) {
  return didJWT.registerLife(url);
}

/**
 *  Decodes JWT without verifying
 *
 *  @example
 *  decodeJWT("eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NksifQ.eyJleHAiOjEsI...")
 *
 *  @param    {String}            jwt                a JSON Web Token to verify
 *  @return   {Object}                               a JS object representing the decoded JWT
 */
function decodeJWT(jwt): {} {
  return didJWT.decodeJWT(jwt);
}

export {
  decodeJWT,
  registerLife,
  verifyJWT,
  createJWT,
  parseDID,
  createDID,
  decodeDID,
  validateDID,
  createKeyPair,
  signData,
  verifyData,
  recoverPublicKey
};
