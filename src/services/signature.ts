import { Either, Right, Left } from "monet";
const ethUtils = require("ethereumjs-util");

function _add0x(input: string) {
  if (input.substring(0, 2) !== "0x") {
    return "0x" + input;
  }
  return input;
}

function _toBuffer(input: string): Buffer {
  return Buffer.from(input, "hex");
}

function _remove0x(key: string): string {
  if (key.substring(0, 2) === "0x") {
    return key.split("x")[1];
  }
  return key;
}

function randomString(length: number) {
  const chars = "0123456789abcdef";
  let result = "";
  for (let i = length; i > 0; --i) {
    result += chars[Math.floor(Math.random() * chars.length)];
  }
  return result;
}

function concatData(args: string[]): Either<Error, string> {
  try {
    return Right(
      args.reduce((acc, data) => {
        return acc.concat(data);
      }, "")
    );
  } catch (err) {
    return Left(err);
  }
}

function hashData(data: string): Either<Error, Buffer> {
  try {
    return Right(ethUtils.keccak(data));
  } catch (err) {
    return Left(err);
  }
}

const signHashed = (privateKey: string) => (
  hashedData: Buffer
): Either<Error, string> => {
  try {
    const pk = _toBuffer(privateKey);
    const signed = ethUtils.ecsign(hashedData, pk);
    return Right(_remove0x(ethUtils.toRpcSig(signed.v, signed.r, signed.s)));
  } catch (err) {
    return Left(err);
  }
};

const recoverPublicKey = (signature: string) => (
  hashedData: Buffer
): Either<Error, Buffer> => {
  try {
    const vrsSig = ethUtils.fromRpcSig(_add0x(signature));
    return Right(ethUtils.ecrecover(hashedData, vrsSig.v, vrsSig.r, vrsSig.s));
  } catch (err) {
    return Left(err);
  }
};

export { randomString, concatData, hashData, recoverPublicKey, signHashed };
