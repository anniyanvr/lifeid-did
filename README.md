[didexample]: ./didExample.png
[lifeidlogo]: ./lifeid_logo.png

# lifeID Decentralized Identifier (DID)

The lifeID platform uses pairwise decentralized identifiers ("DIDs") for people and organizations to identify themselves to one another. This repository is a suite of tools allowing anyone to create, encode, decode DIDs for any blockchain. Please note that this is a work in progress and will almost certainly change over time. Expect revisions as we iron out bugs and improve the developer experience.

A lifeID DID looks like this:

`"did:life:Lo3gVgN4WnXe6CnVn1zFSXGK4wL6eMJ7W1"`

There are three parts to a lifeID did:

1.  The DID prefix `did:` This indicates that you are presenting a standardized DID according to the specification outlined by the World Wide Web Consortium ("W3C").

2.  The DID method. `life:` This signals to everyone that you are presenting a DID for use on the lifeID platform.

When decoded, a DID string includes the following parts:

* The version number. Currently this is a single one byte version number it will start at 1 and can be updated in the future.

* The network ID. This part of the DID describes which blockchain stores its public key. It is 4 bytes long.

* The public key. Different blockchains use different encryption schemes, but they all require a cryptographic keys to prove ownership. It 65 bytes long.

* The checksum. A SHA3-256 digest of the version number, network ID and public key. The checksum provides a way to quickly verify whether the DID is correct or has been changed in any way.

An example decoded DID:

![didExample]

## API

This library provides the following functions for working with DIDs

### `createDID(didObject)`

This function takes a valid DID object and returns an encoded DID wrapped in a promise

#### `input: didObject`

An object with the following fields. All are required.

```javascript
  didObject: {
    method: string,
    network: string,
    version: string,
    key: string
    }
```

#### return value : `Promise(did: string)`

### `decodeDID(did)`

This function takes a valid DID string and returns a DID object wrapped in a promise.

#### `did`

A string that is a validly formatted DID.

#### return value : `Promise(didObject: object)`

### `validateDID(did)`

This function takes a valid DID string and returns a promise. The promise will resove to `true` if and only if the DID is correctly formed.

#### `did`

A string that is a validly formatted DID.

return value : `Promise(boolean)`

![lifeidlogo]

Built with ️
❤️ in Seattle.

Special thanks to the team at [uPort](https://www.uport.me/), our work borrows heavily from their ideas.
