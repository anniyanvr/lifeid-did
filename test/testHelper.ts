import { expect } from "chai";

function testError(data: any, func: any, errorMessage: string) {
  return Promise.resolve(data)
    .then(func)
    .then(() => {
      throw new Error("This was not supposed to succeed");
    })
    .catch(err => {
      expect(err.message).to.equal(errorMessage);
    });
}

const isErrorWithMessage = message => result => {
  result.should.exist; // tslint:disable-line:no-unused-expression
  result.should.be.an.instanceOf(Error);
  result.message.should.equal(message);
};
export { testError, isErrorWithMessage };
